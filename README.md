# Names Cologne Svelte App

a simple [svelte](https://svelte.dev/) app using [Chart.js](https://www.chartjs.org/) to visualize open data of given names in the city of cologne

try it [here on gitlab pages](https://cbusse.gitlab.io/names-cologne-svelte-app/)
